<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'  => 1,
            'name'     => 'John Admin',
            'username' => 'admin',
            'email'    => 'admin@blog-system.com',
            'password' => bcrypt('admin'),
        ]);

        DB::table('users')->insert([
            'role_id'  => 2,
            'name'     => 'Jane Author',
            'username' => 'author',
            'email'    => 'author@blog-system.com',
            'password' => bcrypt('author'),
        ]);
    }
}
