@extends('layouts.frontend.layout')
@section('title')
{{ $post->title }}
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/frontend/css/single-post/styles.css') }}">
<link rel="stylesheet" href="{{ asset('assets/frontend/css/single-post/responsive.css') }}">
@endpush

@section('main-content')

<div class="slider" style="background-image: url({{ Storage::url('post/' . $post->image) }})">
    <div class="display-table  center-text">
        <h1 class="title display-table-cell"><b>{{ $post->title }}</b></h1>
    </div>
</div><!-- slider -->

<section class="post-area section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 no-right-padding">
                <div class="main-post">
                    <div class="blog-post-inner">
                        <div class="post-info">
                            <div class="left-area">
                                <a class="avatar" href="#">
                                    <img src="{{ Storage::url('user/' . $post->user->image) }}">
                                </a>
                            </div>
                            <div class="middle-area">
                                <a class="name" href="#"><b>{{ $post->user->title }}</b></a>
                                <h6 class="date">on {{ $post->created_at->toFormattedDateString() }}</h6>
                            </div>
                        </div><!-- post-info -->

                        <h3 class="title"><b>{{ $post->title }}</b></h3>
                        <div class="post-content">{!! $post->body !!}</div>

                        <ul class="tags">
                            @foreach ($post->categories as $category)
                            <li><a href="{{ route('category.posts', $category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- blog-post-inner -->

                    <div class="post-icons-area">
                        <ul class="post-icons">
                            <li>
                                @guest
                                <a href="javascript:void(0)"
                                    onclick="toastr['info']('You have to log in first', 'Sign In')"><i
                                        class="ion-heart"></i>{{ $post->favourite_to_users()->count() }}</a>
                                @else
                                <a href="javascript:void(0)"
                                    onclick="event.preventDefault();document.querySelector('#favourite-form-{{ $post->id }}').submit()"><i
                                        class="ion-heart {{ auth()->user()->favourite_posts()->where('post_id', $post->id)->count() == 0 ? '' : 'favourite_post'  }}"></i>{{ $post->favourite_to_users->count() }}
                                </a>
                                <form action="{{ route('post.favourite', $post->id) }}" method="POST" class="d-none"
                                    id="favourite-form-{{$post->id}}">
                                    @csrf
                                </form>
                                @endguest
                            </li>

                            <li><a href="{{ route('post.details', $post->slug) . '/#single-comment-section' }}"><i
                                        class="ion-chatbubble"></i>{{ $post->comments->count() }}</a></li>
                            <li><a href="#"><i class="ion-eye"></i>{{ $post->view_count }}</a></li>
                        </ul>
                        <ul class="icons">
                            <li>SHARE : </li>
                            <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                        </ul>
                    </div>

                    <div class="post-footer post-info">
                        <div class="left-area">
                            <a class="avatar"
                                href="{{ $post->user->role_id == 1 ? route('admin.dashboard') : route('author.dashboard') }}">
                                <img src="{{ Storage::url('user/' . $post->user->image ) }}"
                                    alt="{{ $post->user->name }}">
                            </a>
                        </div>
                        <div class="middle-area">
                            <a class="name"
                                href="{{ $post->user->role_id == 1 ? route('admin.dashboard') : route('author.dashboard') }}">
                                <b>{{ $post->user->name }}</b>
                            </a>
                            <h6 class="date">on {{ $post->created_at->toFormattedDateString() }}</h6>
                        </div>
                    </div><!-- post-info -->
                </div><!-- main-post -->
            </div><!-- col-lg-8 col-md-12 -->

            <div class="col-lg-4 col-md-12 no-left-padding">
                <div class="single-post info-area">
                    <div class="sidebar-area about-area">
                        <h4 class="title"><b>{{ $post->user->name }}</b></h4>
                        <p>{{ $post->user->about }}</p>
                    </div>
                    <div class="sidebar-area subscribe-area">
                        <h4 class="title"><b>SUBSCRIBE</b></h4>
                        <div class="input-area">
                            <form method="POST" action="{{ route('subscriber.store') }}">
                                @csrf
                                <input class="email-input" type="email" name="email" placeholder="Enter your email">
                                <button class="submit-btn" type="submit"><i
                                        class="icon ion-ios-email-outline"></i></button>
                            </form>
                        </div>
                    </div><!-- subscribe-area -->

                    <div class="tag-area">
                        <h4 class="title"><b>TAG CLOUD</b></h4>
                        <ul>
                            @foreach ($post->tags as $tag)
                            <li><a href="{{ route('tag.posts', $tag->slug ) }}">{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- subscribe-area -->
                </div><!-- info-area -->
            </div><!-- col-lg-4 col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- post-area -->


<section class="recomended-area section">
    <div class="container">
        <div class="row">
            @foreach ($randomPosts as $rpost)

            <div class="col-lg-4 col-md-6">
                <div class="card h-100">
                    <div class="single-post post-style-1">
                        <div class="blog-image"><img src="{{ Storage::url( 'post/' . $rpost->image ) }}"
                                alt="{{ $rpost->title }}"></div>
                        <a class="avatar"
                            href="{{ $rpost->user->role_id == 1 ? route('admin.dashboard') : route('author.dashboard') }}">
                            <img src="{{ Storage::url('user/' . $post->user->image ) }}"
                                alt="{{ $rpost->user->name }}"></a>
                        <div class="blog-info">
                            <h4 class="title">
                                <a href="{{ route('post.details', $rpost->slug) }}">
                                    <b>{{ $rpost->title }}</b>
                                </a>
                            </h4>
                            <ul class="post-footer">
                                <li>
                                    @guest
                                    <a href="javascript:void(0)"
                                        onclick="toastr['info']('You have to log in first', 'Sign In')"><i
                                            class="ion-heart"></i>{{ $rpost->favourite_to_users()->count() }}</a>
                                    @else
                                    <a href="javascript:void(0)"
                                        onclick="event.preventDefault();document.querySelector('#favourite-form-{{ $rpost->id }}').submit()"><i
                                            class="ion-heart {{ auth()->user()->favourite_posts()->where('post_id', $rpost->id)->count() == 0 ? '' : 'favourite_post'  }}"></i>{{ $post->favourite_to_users->count() }}
                                    </a>
                                    <form action="{{ route('post.favourite', $rpost->id) }}" method="POST"
                                        class="d-none" id="favourite-form-{{$rpost->id}}">
                                        @csrf
                                    </form>
                                    @endguest
                                </li>

                                <li><a href="{{ route('post.details', $rpost->slug) . '/#single-comment-section' }}"><i
                                            class="ion-chatbubble"></i>{{ $rpost->comments->count() }}</a></li>
                                <li><a href="#"><i class="ion-eye"></i>{{ $rpost->view_count }}</a></li>
                            </ul>
                        </div><!-- blog-info -->
                    </div><!-- single-post -->
                </div><!-- card -->
            </div><!-- col-md-6 col-sm-12 -->

            @endforeach
        </div><!-- row -->

    </div><!-- container -->
</section>

<section class="comment-section" id="single-comment-section">
    <div class="container">

        <h4><b>POST COMMENT</b></h4>
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="comment-form">
                    @guest
                    <p>Please <a href="{{route('login')}}">Log in</a> to comment</p>
                    @else
                    <form method="POST" action="{{ route('comment.store', $post->id) }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea name="comment" rows="2" class="text-area-messge form-control"
                                    placeholder="Enter your comment" aria-required="true"
                                    aria-invalid="false"></textarea>
                            </div><!-- col-sm-12 -->
                            <div class="col-sm-12">
                                <button class="submit-btn" type="submit" id="form-submit"><b>POST COMMENT</b></button>
                            </div><!-- col-sm-12 -->

                        </div><!-- row -->
                    </form>
                    @endguest
                </div><!-- comment-form -->

                @if ($post->comments->count())
                <h4><b>COMMENTS ({{ $post->comments->count() }})</b></h4>

                <div class="commnets-area">
                    @foreach ($post->comments as $comment)
                    <div class="comment">
                        <div class="post-info">
                            <div class="left-area">
                                <a class="avatar" href="#">
                                    <img src="{{ Storage::url('user/' . $comment->user->image) }}"
                                        alt="{{ $comment->user->name }}" />
                                </a>
                            </div>
                            <div class="middle-area">
                                <a class="name" href="#"><b>{{ $comment->user->name }}</b></a>
                                <h6 class="date"> on {{ $comment->created_at->diffForHumans() }}</h6>
                            </div>
                        </div><!-- post-info -->
                        {{ $comment->comment }}
                    </div>
                    @endforeach
                </div><!-- commnets-area -->
                @else
                <p>No comments yet.</p>
                @endif

            </div><!-- col-lg-8 col-md-12 -->

        </div><!-- row -->


    </div><!-- container -->
</section>

@endsection

@push('js')

@endpush