@extends('layouts.backend.layout')

@section('title', 'Add a new tag')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert/sweetalert.css') }}">
@endpush

@section('main-content')
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="text-center">
                        ADD NEW TAG
                    </h2>
                </div>
                <div class="body">
                    <form method="POST" action="{{ route('admin.tag.store') }}">
                        @csrf
                        <label for="name">Tag Name</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="name" name="name" class="form-control"
                            placeholder="Enter your tag name" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <a href="{{ route('admin.tag.index') }}" class="btn btn-danger waves-effect">BACK</a>
                        <button type="submit" class="btn btn-primary waves-effect">ADD TAG</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Vertical Layout -->
</div>
@endsection

@push('js')
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>
@endpush