@extends('layouts.backend.layout')

@section('title')
{{ $post->title }}
@endsection


@push('css')
<!-- Sweet Alert 2 Css -->
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert/sweetalert.css') }}">
@endpush

@section('main-content')
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <a href="{{ route('admin.post.index') }}" class="post-edit btn btn-success waves-effect btn-sm"
                        title="Show Post">
                        <i class="material-icons">arrow_back</i>
                        <span>Back</span>
                    </a>
                    <a href="{{ route('admin.post.edit', $post->id) }}"
                        class="post-edit btn btn-info waves-effect btn-sm pull-right" title="Edit Post">
                        <i class="material-icons">edit</i>
                        <span>Edit Post</span>
                    </a>
                    @if ($post->is_approved == false )
                    <button type="submit" class="btn btn-success pull-right m-r-10"
                        onclick="approvePost({{ $post->id }})">
                        <i class="material-icons">done</i>
                        <span>Approve?</span>
                    </button>
                    @else
                    <button type="submit" class="btn btn-danger pull-right m-r-10"
                        onclick="approvePost({{ $post->id }})">
                        <i class="material-icons">security</i>
                        <span>Disapprove?</span>
                    </button>
                    @endif
                    <form method="POST" action="{{ route('admin.post.approve', $post->id) }}" id="post-approve-form"
                        class="d-none">
                        @csrf
                        @method('PATCH')
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ $post->title }}
                        <small>Posted By: <strong><a href="#">{{ $post->user->name }}</a></strong>, Created At:
                            <strong>{{ $post->created_at->toFormattedDateString() }}</strong></small>
                    </h2>
                </div>
                <div class="body">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue">
                    <h2>
                        Categories
                    </h2>
                </div>
                <div class="body">
                    @foreach ($post->categories as $category)
                    <span class="label bg-blue">{{ $category->name }}</span>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header bg-green">
                    <h2>
                        Tags
                    </h2>
                </div>
                <div class="body">
                    @foreach ($post->tags as $tag)
                    <span class="label bg-green">{{ $tag->name }}</span>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header bg-amber">
                    <h2>
                        Featured Image
                    </h2>
                </div>
                <div class="body">
                    <img class="img-responsive thumbnail" src="{{ Storage::url('post/' . $post->image) }}"
                        alt="{{ $post->title }}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.17.1/dist/sweetalert2.all.min.js"></script>
<!-- TinyMCE -->
<script src="{{ asset('assets/backend/plugins/tinymce/tinymce.js') }}"></script>
<script type="text/javascript">
    $(function () {
    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
        });
        tinymce.suffix = ".min";
        tinyMCE.baseURL = '{{ asset('assets/backend/plugins/tinymce')}}';
    });

        function approvePost() {
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You want to change Approval Status",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Change it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
        }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons.fire(
            'Status Changed!',
            'Posts Approval Status has been Changed :)',
            'success'
            );
            event.preventDefault();
            document.querySelector('#post-approve-form').submit();
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'Failed to Change Aproval Status ):',
            'error'
            )
        }
        })
    }
</script>
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>
{{-- <script src="{{ asset('assets/backend/js/pages/forms/advanced-form-elements.js') }}"></script> --}}
<!-- Custom Js -->
@endpush