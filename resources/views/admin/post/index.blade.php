@extends('layouts.backend.layout')

@section('title', 'Posts')

@push('css')
<link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}"
    rel="stylesheet">
@endpush

@section('main-content')
<div class="container-fluid">
    <div class="block-header">
        <a href="{{ route('admin.post.create') }}" class="btn btn-primary waves-effect m-b-10 m-t-20">
            <i class="material-icons">add</i>
            <span>Add New Post</span>
        </a>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        ALL POSTS
                        <span class="badge bg-blue">{{ $posts->count() }}</span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th class="text-right">
                                        <i class="material-icons">visibility</i>
                                    </th>
                                    <th>Status</th>
                                    <th>Is Approved</th>
                                    <th>Thumbnail</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Index</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th class="text-center">
                                        <i class="material-icons">visibility</i>
                                    </th>
                                    <th>Status</th>
                                    <th>Is Approved</th>
                                    <th>Thumbnail</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($posts as $key=>$post)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ str_limit($post->title, 15) }}</td>
                                    <td>{{ $post->user->name }}</td>
                                    <td class="text-center">{{ $post->view_count }}</td>
                                    <td>
                                        @if ($post->status == true)
                                        <span class="badge bg-green">Published</span>
                                        @else
                                        <span class="badge bg-blue">Pending</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($post->is_approved == true)
                                        <span class="badge bg-green">Approved</span>
                                        @else
                                        <span class="badge bg-blue">Pending</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <img width="60" src="{{ Storage::url('post/'. $post->image ) }}"
                                            alt="{{ $post->name }}" />
                                    </td>
                                    <td>{{ $post->created_at }}</td>
                                    <td>{{ $post->updated_at }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.post.show', $post->id) }}"
                                            class="post-edit btn btn-success waves-effect btn-sm" title="Show Post">
                                            <i class="material-icons">visibility</i>
                                        </a>
                                        <a href="{{ route('admin.post.edit', $post->id) }}"
                                            class="post-edit btn btn-info waves-effect btn-sm" title="Edit Post">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button class="btn btn-danger btn-sm waves-effect" type="submit"
                                            title="Delete Post">
                                            <i class="material-icons" onclick="deletePost({{$post->id}})">delete</i>
                                        </button>
                                        <form id="post-delete-form-{{$post->id}}"
                                            action="{{ route('admin.post.destroy', $post->id) }}" method="POST"
                                            class="d-none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
@endsection

@push('js')
<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}">
</script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.17.1/dist/sweetalert2.all.min.js"></script>

<!-- Custom Js -->
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>

<!-- Demo Js -->
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>

<!-- Delete Post with Notification Js -->
<script type="text/javascript">
    function deletePost(catId) {
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
        }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons.fire(
            'Deleted!',
            'Post has been deleted :)',
            'success'
            );
            event.preventDefault();
            document.querySelector('#post-delete-form-'+ catId).submit();
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'Failed to delete Post ):',
            'error'
            )
        }
        })
    }
</script>

@endpush