@extends('layouts.backend.layout')

@section('title')
{{ $post->title }}
@endsection


@push('css')
<!-- Sweet Alert 2 Css -->
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert/sweetalert.css') }}">
@endpush

@section('main-content')
<div class="container-fluid">
    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <a href="{{ route('author.post.index') }}" class="post-edit btn btn-success waves-effect btn-sm"
                        title="Show Post">
                        <i class="material-icons">arrow_back</i>
                        <span>Back</span>
                    </a>
                    <a href="{{ route('author.post.edit', $post->id) }}"
                            class="post-edit btn btn-info waves-effect btn-sm pull-right" title="Edit Post">
                            <i class="material-icons">edit</i>
                            <span>Edit Post</span>
                        </a>
                    @if ($post->is_approved == false )
                    <button type="button" class="btn btn-danger pull-right m-r-10">
                        <i class="material-icons">hourglass_full</i>
                        <span>Pending</span>
                    </button>
                    @else
                    <button type="button" class="btn btn-success pull-right m-r-10" disabled>
                        <i class="material-icons">done</i>
                        <span>Approved</span>
                    </button>
                    @endif
                    
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ $post->title }}
                        <small>Posted By: <strong><a href="#">{{ $post->user->name }}</a></strong>, Created At:
                            <strong>{{ $post->created_at->toFormattedDateString() }}</strong></small>
                    </h2>
                </div>
                <div class="body">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue">
                    <h2>
                        Categories
                    </h2>
                </div>
                <div class="body">
                    @foreach ($post->categories as $category)
                    <span class="label bg-blue">{{ $category->name }}</span>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header bg-green">
                    <h2>
                        Tags
                    </h2>
                </div>
                <div class="body">
                    @foreach ($post->tags as $tag)
                    <span class="label bg-green">{{ $tag->name }}</span>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header bg-amber">
                    <h2>
                        Featured Image
                    </h2>
                </div>
                <div class="body">
                    <img class="img-responsive thumbnail" src="{{ Storage::url('post/' . $post->image) }}"
                        alt="{{ $post->title }}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<!-- Multi Select Plugin Js -->
{{-- <script src="{{ asset('assets/backend/plugins/multi-select/js/jquery.multi-select.js') }}"></script> --}}
<!-- Select Plugin Js -->
<script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- TinyMCE -->
<script src="{{ asset('assets/backend/plugins/tinymce/tinymce.js') }}"></script>
<script type="text/javascript">
    $(function () {
    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
        });
        tinymce.suffix = ".min";
        tinyMCE.baseURL = '{{ asset('assets/backend/plugins/tinymce')}}';
    });
</script>
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>
{{-- <script src="{{ asset('assets/backend/js/pages/forms/advanced-form-elements.js') }}"></script> --}}
<!-- Custom Js -->
@endpush