@extends('layouts.backend.layout')

@section('title', 'Add a new Post')

@push('css')
<!-- Sweet Alert 2 Css -->
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert/sweetalert.css') }}">
<!-- Multi Select Css -->
<link href="{{ asset('assets/backend/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">
<!-- Bootstrap Select Css -->
<link href="{{ asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('main-content')
<div class="container-fluid">
    <form method="POST" action="{{ route('author.post.store') }}" enctype="multipart/form-data">
        @csrf
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ADD A NEW POST
                        </h2>
                    </div>
                    <div class="body">
                        <label for="name">Add Title</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="title" name="title" class="form-control"
                                    value="{{ old('title') }}" placeholder="Enter your post title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">Featured Image</label>
                            <div class="form-group">
                                <input type="file" id="image" name="image" class="form-control" accept="image/*">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="publish" class="filled-in" name="status">
                            <label for="publish">Publish</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            CATEGORIES AND TAGS
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line {{ $errors->has('categories') ? 'focused error' : '' }}">
                                <p><b>Select Categories</b></p>
                                <select class="form-control show-tick" data-live-search="true" name="categories[]"
                                    multiple>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line {{ $errors->has('tags') ? 'focused error' : '' }}">
                                <p><b>Select Tags</b></p>
                                <select class="form-control show-tick" data-live-search="true" name="tags[]" multiple>
                                    @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <a href="{{ route('author.post.index') }}" class="btn btn-danger waves-effect">
                                <i class="material-icons">arrow_back</i>
                                <span>BACK</span>
                            </a>
                            <button type="submit" class="btn btn-primary waves-effect pull-right">
                                <i class="material-icons">check_circle</i>
                                <span>Submit</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- TinyMCE -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Body
                        </h2>
                    </div>
                    <div class="body">
                    <textarea id="tinymce" name="body">{{ old('body') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# TinyMCE -->
    </form>
</div>
@endsection

@push('js')
<!-- Multi Select Plugin Js -->
{{-- <script src="{{ asset('assets/backend/plugins/multi-select/js/jquery.multi-select.js') }}"></script> --}}
<!-- Select Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- TinyMCE -->
<script src="{{ asset('assets/backend/plugins/tinymce/tinymce.js') }}"></script>
<script type="text/javascript">
    $(function () {
    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
        });
        tinymce.suffix = ".min";
        tinyMCE.baseURL = '{{ asset('assets/backend/plugins/tinymce')}}';
    });
</script>
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>
{{-- <script src="{{ asset('assets/backend/js/pages/forms/advanced-form-elements.js') }}"></script> --}}
<!-- Custom Js -->
@endpush