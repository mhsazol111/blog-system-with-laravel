@extends('layouts.backend.layout')

@section('title', 'Comments')

@push('css')
<link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}"
    rel="stylesheet">
@endpush

@section('main-content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        ALL COMMENTS
                        <span class="badge bg-blue"></span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Comment Info</th>
                                    <th>Post Info</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Comment Info</th>
                                    <th>Post Info</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($posts as $key=>$post)
                                    @foreach ($post->comments as $comment)
                                        {{-- @if ( $comment->user == auth()->user() ) --}}
                                            <tr>
                                                <td>
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object"
                                                                    src="{{ Storage::url('user/' . $comment->user->image) }}"
                                                                    width="64" height="64">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">{{ $comment->user->name }}
                                                                <small>{{ $comment->created_at->diffForHumans() }}</small>
                                                            </h4>
                                                            <p>{{ $comment->comment }}</p>
                                                            <a target="_blank"
                                                                href="{{ route('post.details',$comment->post->slug . '#single-comment-section') }}">Reply</a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a target="_blank"
                                                                href="{{ route( 'post.details', $comment->post->slug ) }}">
                                                                <img class="media-object"
                                                                    src="{{ Storage::url('post/' .$comment->post->image ) }}"
                                                                    width="64" height="64">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <a target="_blank"
                                                                href="{{ route('post.details',$comment->post->slug) }}">
                                                                <h4 class="media-heading">
                                                                    {{ str_limit( $comment->post->title, '40' ) }}</h4>
                                                            </a>
                                                            <p>by <strong>{{ $comment->post->user->name }}</strong></p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-danger waves-effect"
                                                        onclick="deleteComment({{ $comment->id }})">
                                                        <i class="material-icons">delete</i>
                                                    </button>
                                                    <form id="comment-delete-form-{{ $comment->id }}" method="POST"
                                                        action="{{ route('author.comment.destroy', $comment->id) }}" class="d-none">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </td>
                                            </tr>
                                        {{-- @endif --}}
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
@endsection

@push('js')
<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}">
</script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.17.1/dist/sweetalert2.all.min.js"></script>

<!-- Custom Js -->
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>
<script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>

<!-- Demo Js -->
<script src="{{ asset('assets/backend/js/demo.js') }}"></script>

<!-- Delete Tag with Notification Js -->
<script type="text/javascript">
    function deleteComment(commentId) {
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
        })

        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
        }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons.fire(
            'Deleted!',
            'Comment has been deleted :)',
            'success'
            );
            event.preventDefault();
            document.querySelector('#comment-delete-form-'+ commentId).submit();
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'Failed to delete Comment ):',
            'error'
            )
        }
        })
    }
</script>

@endpush