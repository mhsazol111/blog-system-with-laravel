<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Common Css Files -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/ionicons.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset( 'assets/frontend/css/common.css') }}">
    <link rel="stylesheet" href="{{ asset( 'assets/frontend/css/responsive.css') }}">

    <!-- Toster Css -->
    @toastr_css


    @stack('css')

    <!-- Jquery --->
    <script src="{{ asset('assets/frontend/js/jquery-3.4.1.min.js') }}"></script>

</head>

<body>
    <div id="page-content">
        @include('layouts.frontend.partials.header')
        <div id="main-content">
            @yield('main-content')
        </div>
        @include('layouts.frontend.partials.footer')
    </div>

    
    <!-- Stack Js -->
    @stack('js')

    <script src="{{ asset('assets/frontend/js/tether.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>

    <!-- Toster Js -->
    @toastr_js
    @toastr_render
    <!-- Toster Error Handeling -->
    <script>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
            toastr["error"]("{{ $error }}", "Error");
            @endforeach
        @endif
    </script>
</body>

</html>