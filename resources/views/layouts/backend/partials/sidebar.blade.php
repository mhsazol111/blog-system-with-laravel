<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ Storage::url('user/' . auth()->user()->image ) }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ auth()->user()->name }}</div>
            <div class="email">{{ auth()->user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a
                            href="{{ auth()->user()->role_id == 1 ? route('admin.settings') : route('author.settings') }}"><i
                                class="material-icons">person</i>Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Sign Out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        @if ( request()->is('admin*') )
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ request()->is('admin/dashboard') ? 'active' : '' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/tag*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">label</i>
                    <span>Tags</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ request()->is('admin/tag') ? 'active' : '' }}">
                        <a href="{{ route('admin.tag.index') }}">
                            All Tags
                        </a>
                    </li>
                    <li class="{{ request()->is('admin/tag/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.tag.create') }}">
                            Add a New Tag
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/category*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">category</i>
                    <span>Category</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ request()->is('admin/category') ? 'active' : '' }}">
                        <a href="{{ route('admin.category.index') }}">
                            All Categories
                        </a>
                    </li>
                    <li class="{{ request()->is('admin/category/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.category.create') }}">
                            Add a New Category
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/post*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">library_books</i>
                    <span>Posts</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ request()->is('admin/post') ? 'active' : '' }}">
                        <a href="{{ route('admin.post.index') }}">
                            All Posts
                        </a>
                    </li>
                    <li class="{{ request()->is('admin/post/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.post.create') }}">
                            Add a New Post
                        </a>
                    </li>
                    <li class="{{ request()->is('admin/post/pending') ? 'active' : '' }}">
                        <a href="{{ route('admin.post.pending') }}">
                            Pending Posts
                        </a>
                    </li>
                    <li class="{{ request()->is('admin/post/favourite') ? 'active' : '' }}">
                        <a href="{{ route('admin.post.favourite') }}">
                            Favourite Posts
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/subscriber') ? 'active' : '' }}">
                <a href="{{ route('admin.subscriber.index') }}">
                    <i class="material-icons">account_box</i>
                    <span>Subscribers</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/comment') ? 'active' : '' }}">
                <a href="{{ route('admin.comment.index') }}">
                    <i class="material-icons">comment</i>
                    <span>Comments</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/authors') ? 'active' : '' }}">
                <a href="{{ route('admin.author.index') }}">
                    <i class="material-icons">perm_identity</i>
                    <span>Authors</span>
                </a>
            </li>
            <li class="header">System</li>
            <li class="{{ request()->is('admin/settings') ? 'active' : '' }}">
                <a href="{{ route('admin.settings') }}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
            </li>
            <li>
                <a href="{{ route('home') }}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="material-icons">input</i>
                    <span>Log Out</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        @elseif( request()->is('author*') )
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ request()->is('author/dashboard') ? 'active' : '' }}">
                <a href="{{ route('author.dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ request()->is('author/post*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">library_books</i>
                    <span>Posts</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ request()->is('author/post') ? 'active' : '' }}">
                        <a href="{{ route('author.post.index') }}">
                            All Posts
                        </a>
                    </li>
                    <li class="{{ request()->is('author/post/create') ? 'active' : '' }}">
                        <a href="{{ route('author.post.create') }}">
                            Add a New Post
                        </a>
                    </li>
                    <li class="{{ request()->is('author/post/favourite') ? 'active' : '' }}">
                        <a href="{{ route('author.post.favourite') }}">
                            Favourite Posts
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('author/comment') ? 'active' : '' }}">
                <a href="{{ route('author.comment.index') }}">
                    <i class="material-icons">comment</i>
                    <span>Comments</span>
                </a>
            </li>
            <li class="header">System</li>
            <li class="{{ request()->is('author/settings') ? 'active' : '' }}">
                <a href="{{ route('author.settings') }}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
            </li>
            <li>
                <a href="{{ route('home') }}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="material-icons">input</i>
                    <span>Log Out</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        @endif

    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
