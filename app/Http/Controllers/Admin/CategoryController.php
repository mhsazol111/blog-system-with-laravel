<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category)
    {
        $categories = $category->latest()->get();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->validate($request, [
            'name'   => 'required | min:2 | unique:categories',
            'image'  => 'mimes:jpeg,bmp,png,jpg,webp',
        ]);

        //get form image
        $image = $request->file('image');
        $slug  = str_slug($request->name);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('category/banners')) {
                Storage::disk('public')->makeDirectory('category/banners');
            }
            //Resize Banner image for category

            $categoryBanner = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('category/banners/' . $imageName, $categoryBanner);

            //Check category/thumb Directory exists
            if (!Storage::disk('public')->exists('category/thumbnails')) {
                Storage::disk('public')->makeDirectory('category/thumbnails');
            }
            //Resize thumbnails image for category
            $categoryThumb = Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('category/thumbnails/' . $imageName, $categoryThumb);
        } else {
            $imageName = 'default.png';
        }

        $category        = new Category();
        $category->name  = $request->name;
        $category->slug  = str_slug($request->name);
        $category->image = $imageName;

        $category->save();

        toastr()->success('Category Successfully Added', 'Created');

        return redirect(route('admin.category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name'   => 'required | min:2',
            'image'  => 'mimes:jpeg,bmp,png,jpg,webp',
        ]);

        //get form image
        $image = $request->file('image');
        $slug  = str_slug($request->name);


        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('category/banners')) {
                Storage::disk('public')->makeDirectory('category/banners');
            }
            //Delete Old image
            if (Storage::disk('public')->exists('category/banners/'. $category->image)) {
                Storage::disk('public')->delete('category/banners/' . $category->image);
            }
            //Resize Banner image for category
            $categoryBanner = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('category/banners/' . $imageName, $categoryBanner);

            //Check category/thumb Directory exists
            if (!Storage::disk('public')->exists('category/thumbnails')) {
                Storage::disk('public')->makeDirectory('category/thumbnails');
            }
            //Delete Old image
            if (Storage::disk('public')->exists('category/thumbnails/'. $category->image)) {
                Storage::disk('public')->delete('category/thumbnails/' . $category->image);
            }
            //Resize thumb image for category
            $categoryThumb = Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('category/thumbnails/' . $imageName, $categoryThumb);
        } else {
            $imageName = $category->image;
        }

        $category->name  = $request->name;
        $category->slug  = str_slug($request->name);
        $category->image = $imageName;

        $category->update();

        toastr()->success('Category Successfully Updated', 'Updated');

        return redirect(route('admin.category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        
        if (Storage::disk('public')->exists('category/banners/'. $category->image)) {
            Storage::disk('public')->delete('category/banners/' . $category->image);
        }
        if (Storage::disk('public')->exists('category/thumbnails/'. $category->image)) {
            Storage::disk('public')->delete('category/thumbnails/' . $category->image);
        }

        toastr()->success('Category Successfully Deleted', 'Deleted');
        return back();
    }
}
