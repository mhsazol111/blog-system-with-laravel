<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index(Comment $comment)
    {
        $comments = $comment->latest()->get();
        return view('admin.comment', compact('comments'));
    }

    public function destroy(Comment $comment) 
    {
        $comment->delete();
        toastr()->success('Comment Successfully Deleted', 'Deleted');

        return back();
    }
}
