<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\AuthorPostApproved;
use App\Notifications\SubscriberPostNotification;
use App\Subscriber;
use App\Tag;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $posts = $post->latest()->get();
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category, Tag $tag)
    {
        $categories = $category->all();
        $tags       = $tag->all();
        return view('admin.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post, Category $category, Tag $tag)
    {
        $this->validate($request, [
            'title'           => 'required',
            'image'           => 'required',
            'categories'      => 'required',
            'tags'            => 'required',
            'body'            => 'required',
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            //Resize Banner image for category
            $postFeaturedImage = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('post/' . $imageName, $postFeaturedImage);
        } else {
            $imageName = 'default.png';
        }

        $post           = new Post();
        $post->user_id  = auth()->id();
        $post->title    = $request->title;
        $post->slug     = $slug;
        $post->image    = $imageName;
        $post->body     = $request->body;
        if ($request->has('status')) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->is_approved = true;

        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        //Notification Send to subscriber
        $subscribers = Subscriber::all();
        foreach ($subscribers as $subscriber) {
            Notification::route('mail', $subscriber->email)
            ->notify(new SubscriberPostNotification($post));
        }

        toastr()->success('Post Successfully Added', 'Created');

        return redirect(route('admin.post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, Tag $tag, Category $category)
    {
        return view('admin.post.show', compact('post', 'tag', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, Tag $tag, Category $category)
    {
        $categories = $category::all();
        $tags       = $tag::all();
        return view('admin.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title'           => 'required',
            'image'           => 'image',
            'categories'      => 'required',
            'tags'            => 'required',
            'body'            => 'required',
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }
            // Delete Old Thumbnial
            if (Storage::disk('public')->exists('post/' . $post->image)) {
                Storage::disk('public')->delete('post/' . $post->image);
            }

            //Resize Banner image for category
            $postFeaturedImage = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('post/' . $imageName, $postFeaturedImage);
        } else {
            $imageName = $post->image;
        }

        $post->user_id  = auth()->id();
        $post->title    = $request->title;
        $post->slug     = $slug;
        $post->image    = $imageName;
        $post->body     = $request->body;
        if ($request->has('status')) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->is_approved = true;

        $post->update();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        toastr()->success('Post Successfully Updated', 'Updated');

        return redirect(route('admin.post.index'));
    }

    public function pending(Post $post)
    {
        $posts = $post::where('is_approved', false)->get();
        return view('admin.post.pending', compact('posts'));
    }

    public function approval($id)
    {
        $post  = Post::find($id);

        if ($post->is_approved == false) {
            $post->is_approved = true;

            if ($post->user->role_id == 2) {
                $post->user->notify(new AuthorPostApproved($post));
            }
        } else {
            $post->is_approved = false;
        }

        $post->update();

        //Notification Send to subscriber
        $subscribers = Subscriber::all();
        foreach ($subscribers as $subscriber) {
            Notification::route('mail', $subscriber->email)
            ->notify(new SubscriberPostNotification($post));
        }

        toastr()->info('Post Approval Status Changed', 'Changed');

        return back();
    }

    public function favourite(Post $post)
    {
        $posts = auth()->user()->favourite_posts;
        return view('admin.post.favourite', compact('posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (Storage::disk('public')->exists('post/' . $post->image)) {
            Storage::disk('public')->delete('post/' . $post->image);
        }

        $post->categories()->detach();
        $post->tags()->detach();

        $post->delete();

        toastr()->success('Post Successfully Deleted', 'Deleted');
        return back();
    }
}
