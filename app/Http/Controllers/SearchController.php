<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request, Post $post)
    {
        $query = $request->input('query');
        $posts = Post::where('title', 'LIKE', "%$query%")->status()->approved()->paginate(6);
        return view('search', compact('query', 'posts'));
    }
}
