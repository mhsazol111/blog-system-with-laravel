<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    public function details(Post $post, $slug, Category $category)
    {
        $post = Post::where('slug', $slug)->status()->approved()->first();

        $blogKey = 'blog_' . $post->id;
        if (!Session::has($blogKey)) {
            $post->increment('view_count');
            Session::put($blogKey, 1);
        }

        $randomPosts = $post::status()->approved()->take(3)->inRandomOrder()->get();
        return view('post', compact('post', 'randomPosts'));
    }

    public function index(Post $post)
    {
        $posts = $post->latest()->status()->approved()->paginate(6);
        return view('posts', compact('posts'));
    }

    public function postByCategory(Category $category, $slug)
    {
        $category = Category::where('slug', $slug)->first();
        $posts    = $category->posts()->status()->approved()->paginate(6);
        return view('category', compact('category', 'posts'));
    }

    public function postByTag(Tag $tag, $slug)
    {
        $tag   = Tag::where('slug', $slug)->first();
        $posts = $tag->posts()->status()->approved()->paginate(6);
        return view('tag', compact('tag', 'posts'));
    }
}
