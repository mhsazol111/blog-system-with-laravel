<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouritePostController extends Controller
{
    public function add(Post $post, User $user)
    {
        $isFavourite = auth()->user()->favourite_posts()->where('post_id', $post->id)->count();
        // $isFavourite = DB::table('post_user')->where('post_id', $post->id)->count();
        if( $isFavourite == 0 ) {
            auth()->user()->favourite_posts()->attach($post->id);

            toastr()->success('Post Successfully added to your favourite', 'Favourite');
            return back();
        } else {
            auth()->user()->favourite_posts()->detach($post->id);

            toastr()->success('Post Successfully removed from your favourite', 'Favourite');
            return back();
        }
    }
}
