<?php

namespace App\Http\Controllers\Author;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NewAuthorPost;
use App\Tag;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $posts = auth()->user()->posts()->latest()->get();
        return view('author.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category, Tag $tag, Post $post)
    {
        $categories = $category->all();
        $tags       = $tag->all();
        return view('author.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'           => 'required',
            'image'           => 'required',
            'categories'      => 'required',
            'tags'            => 'required',
            'body'            => 'required',
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            //Resize Banner image for category
            $postFeaturedImage = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('post/' . $imageName, $postFeaturedImage);
        } else {
            $imageName = 'default.png';
        }

        $post           = new Post();
        $post->user_id  = auth()->id();
        $post->title    = $request->title;
        $post->slug     = $slug;
        $post->image    = $imageName;
        $post->body     = $request->body;
        if ($request->has('status')) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->is_approved = false;

        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        //Notification Send
        $users = User::where('role_id', 1)->get();
        Notification::send($users, new NewAuthorPost($post));

        toastr()->success('Post Successfully Added', 'Created');

        return redirect(route('author.post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, Tag $tag, Category $category)
    {
        if ($post->user_id != auth()->id()) {
            toastr()->error('You are not Authorized to access this post', 'Unauthorized');
            return back();
        }
        return view('author.post.show', compact('post', 'tag', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, Tag $tag, Category $category)
    {
        if ($post->user_id != auth()->id()) {
            toastr()->error('You are not Authorized to access this post', 'Unauthorized');
            return back();
        }
        $categories = $category::all();
        $tags       = $tag::all();
        return view('author.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if ($post->user_id != auth()->id()) {
            toastr()->error('You are not Authorized to access this post', 'Unauthorized');
            return back();
        }

        $this->validate($request, [
            'title'           => 'required',
            'image'           => 'image',
            'categories'      => 'required',
            'tags'            => 'required',
            'body'            => 'required',
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }
            // Delete Old Thumbnial
            if (Storage::disk('public')->exists('post/' . $post->image)) {
                Storage::disk('public')->delete('post/' . $post->image);
            }

            //Resize Banner image for category
            $postFeaturedImage = Image::make($image)->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('post/' . $imageName, $postFeaturedImage);
        } else {
            $imageName = $post->image;
        }

        $post->user_id  = auth()->id();
        $post->title    = $request->title;
        $post->slug     = $slug;
        $post->image    = $imageName;
        $post->body     = $request->body;
        if ($request->has('status')) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->is_approved = false;

        $post->update();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        toastr()->success('Post Successfully Updated', 'Updated');

        return redirect(route('author.post.index'));
    }

    public function favourite(Post $post)
    {
        $posts = auth()->user()->favourite_posts;
        return view('author.post.favourite', compact('posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->user_id != auth()->id()) {
            toastr()->error('You are not Authorized to access this post', 'Unauthorized');
            return back();
        }

        if (Storage::disk('public')->exists('post/' . $post->image)) {
            Storage::disk('public')->delete('post/' . $post->image);
        }

        $post->categories()->detach();
        $post->tags()->detach();

        $post->delete();

        toastr()->success('Post Successfully Deleted', 'Deleted');
        return back();
    }
}
