<?php

namespace App\Http\Controllers\Author;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('author.settings');
    }

    public function profileupdate(Request $request, User $user)
    {
        $user  = User::findOrFail(auth()->id());

        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required | email',
            'image' => 'image',
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->name);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $request->file('image')->getClientOriginalExtension();

            //Check category/banner Directory exists
            if (!Storage::disk('public')->exists('user')) {
                Storage::disk('public')->makeDirectory('user');
            }
            // Delete Old Thumbnial
            if (Storage::disk('public')->exists('user/' . $user->image)) {
                Storage::disk('public')->delete('user/' . $user->image);
            }

            //Resize Banner image for category
            $postFeaturedImage = Image::make($image)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            Storage::disk('public')->put('user/' . $imageName, $postFeaturedImage);
        } else {
            $imageName = $user->image;
        }

        $user->name  = $request->name;
        $user->email = $request->email;
        $user->image = $imageName;
        $user->about = $request->about;
        $user->update();

        toastr()->success('User Successfully Updated', 'Updated');

        return back();
    }

    public function passwordupdate(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password'     => 'required | confirmed',
        ]);

        $oldPassword = auth()->user()->password;
        if (Hash::check($request->old_password, $oldPassword)) {
            if (Hash::check($request->passsword, $oldPassword)) {
                toastr()->error('New Password Can not be same', 'Matched');
                return back();
            } else {
                $user           = User::find(auth()->id());
                $user->password = Hash::make($request->password);
                $user->update();

                toastr()->success('Password Successfully Updated', 'Updated');

                auth()->logout();
                return back();
            }
        } else {
            toastr()->error('Password Did not match', 'Error');
            return back();
        }
    }
}
