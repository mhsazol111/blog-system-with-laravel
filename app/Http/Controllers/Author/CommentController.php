<?php

namespace App\Http\Controllers\Author;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index(Comment $comment)
    {
        $posts = auth()->user()->posts;
        return view('author.comment', compact('posts'));
    }

    public function destroy(Comment $comment)
    {
        if ($comment->user == auth()->user()) {
            $comment->delete();
            toastr()->success('Comment Successfully Deleted', 'Deleted');
        } else {
            toastr()->error('You don\'t have permission to delete this comment', 'Permission Denied');
        }

        return back();
    }
}
