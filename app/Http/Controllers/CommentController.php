<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $post)
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);

        $comment          = new Comment();
        $comment->post_id = $post;
        $comment->user_id = auth()->user()->id;
        $comment->comment = $request->comment;
        $comment->save();

        toastr()->success('Comment Added', 'Comment');

        return back();
    }
}
