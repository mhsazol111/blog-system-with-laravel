<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\Http\Controllers\Admin\PostController;

Route::get('/', 'HomeController@index')->name('home');

Route::get('posts', 'PostController@index')->name('posts');
Route::get('/post/{slug}', 'PostController@details')->name('post.details');
Route::get('/category/{slug}', 'PostController@postByCategory')->name('category.posts');
Route::get('/tag/{slug}', 'PostController@postByTag')->name('tag.posts');
Route::get('/search', 'SearchController@search')->name('search');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::post('favourite/{post}/add', 'FavouritePostController@add')->name('post.favourite');
    Route::post('post/{post}', 'CommentController@store')->name('comment.store');
});

Route::namespace('Admin')->as('admin.')->prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('settings', 'SettingsController@index')->name('settings');
    Route::patch('profile-update', 'SettingsController@profileupdate')->name('profile.update');
    Route::patch('password-update', 'SettingsController@passwordupdate')->name('password.update');

    Route::resource('tag', 'TagController');
    Route::resource('category', 'CategoryController');

    Route::get('/post/pending', 'PostController@pending')->name('post.pending');
    Route::patch('/post/{id}/approve', 'PostController@approval')->name('post.approve');
    Route::get('/post/favourite', 'PostController@favourite')->name('post.favourite');
    Route::resource('post', 'PostController');

    Route::get('/subscriber', 'SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}', 'SubscriberController@destroy')->name('subscriber.destroy');

    Route::get('/comment', 'CommentController@index')->name('comment.index');
    Route::delete('/comment/{comment}', 'CommentController@destroy')->name('comment.destroy');

    Route::get('/authors', 'AuthorController@index')->name('author.index');
    Route::delete('/authors/{author}', 'AuthorController@destroy')->name('author.destroy');
});

Route::namespace('Author')->as('author.')->prefix('author')->middleware(['auth', 'author'])->group(function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('settings', 'SettingsController@index')->name('settings');
    Route::patch('profile-update', 'SettingsController@profileupdate')->name('profile.update');
    Route::patch('password-update', 'SettingsController@passwordupdate')->name('password.update');

    Route::get('/post/favourite', 'PostController@favourite')->name('post.favourite');
    Route::resource('post', 'PostController');

    Route::get('/comment', 'CommentController@index')->name('comment.index');
    Route::delete('/comment/{comment}', 'CommentController@destroy')->name('comment.destroy');
});

Route::post('subscriber', 'SubscriberController@store')->name('subscriber.store');

View::composer('layouts.frontend.partials.footer', function ($view) {
    $categories = Category::all();
    $view->With(compact('categories'));
});
